import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String WORD_PATTERN = "\\s+";

    public String getResult(String inputStr){
        try {
            List<Input> inputList = getInputs(inputStr);

            inputList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());

            StringJoiner joiner = new StringJoiner("\n");

            inputList.stream()
                    .forEach(input -> {
                        String elementString = input.getValue() + " " + input.getWordCount();
                        joiner.add(elementString);
                    });
            return joiner.toString();
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private List<Input> getInputs(String inputStr) {
        String[] wordsArr = inputStr.split(WORD_PATTERN);

        List<Input> inputList = Arrays.stream(wordsArr)
                .map(word -> new Input(word, 1))
                .collect(Collectors.toList());

        //get the list for the next step of sizing the same word
        List<Input> list = getList(inputList);
        return list;
    }


    private List<Input> getList(List<Input> inputList) {
        Map<String, List<Input>> map = new HashMap<>();

        inputList.forEach(input -> {
            List<Input> inputArr = map.getOrDefault(input.getValue(), new ArrayList<>());
            inputArr.add(input);
            map.put(input.getValue(), inputArr);
        });

        return map.entrySet().stream()
                .map(entry -> new Input(entry.getKey(), entry.getValue().size()))
                .collect(Collectors.toList());
    }
}
